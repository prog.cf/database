**Knapsack problem** is a combinatorial optimization problem which is roughly formulated as follows. You have a knapsack that can contain up to W pounds of stuff, or it will break. You have N things, each of them has weight `w[i]` and cost `c[i]`. You task is to choose which things to put in the knapsack so that it doesn't break, and maximize the total cost of the taken things.

The knapsack problem is [NP-complete](topic:complexity#np), which means it cannot be solved efficiently for large enough thing count or total weight. This problem can be solved quickly enough under any of the following conditions:

- `N < 20`: [algo:knapsackbitmask]
- `N < 40`: [algo:mitm#knapsack]
- `N*W < 10^6`: [algo:knapsackdp]

In some special cases, for example, when `w[i] = c[i]` and, when sorted, `c[i] >= c[1]+c[2]+...+c[i-1]`, greedy solutions give an optimal answer.
