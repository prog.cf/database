**Prefix sums** is a classic algorithm that solves [range sum](problem:rangesum) problem. It can also be adopted for range XOR, range multiplication and [substring hashing](topic:hash).

Consider the following problem.

You are given an array `A` of `N` integers. You have to answer `Q` range sum query. That is, you are given two numbers `L_i` and `R_i` and you have to print `A[L_i] + A[L_i+1] + ... + A[R_i]`.

The `O(NQ)` approach is to handle each query separately:

```cpp
int main() {
	// Input data
	int n, q;
	cin >> n >> q;
	vector<int> a(n);
	for(int i = 0; i < n; i++) {
		cin >> a[i];
	}

	// Handle queries
	for(int i = 0; i < q; i++) {
		int l, r;
		cin >> l >> r;
		l--;
		int ans = 0;
		for(int j = l; j < r; j++) {
			ans += q[j];
		}
		cout << ans << "\n";
	}
}
```

```python
# Input data
n, q = map(int, input().split())
a = list(map(int, input().split()))

# Handle queries
for _ in range(q):
	l, r = map(int, input().split())
	l -= 1
	print(sum(a[l:r]))
```

Prefix sums optimize this solution to `O(N+Q)` as follows. Let's create a new array `P`, called **prefix sum array**, where `P[i] = A[1]+A[2]+...+A[i]`. This array can be calculated in `O(n)` because `P[i] = P[i-1]+A[i]`. Then, `A[l]+...+A[r] = P[r] - P[l-1]` can be calculated in `O(1)`.

For example:

- `A = [1, 7, 3, 2, 5]`
- `P = [1, 8, 11, 13, 18]`
- `A[2] + A[3] + A[4] = 7+3+2 = 12`
- `P[4] - P[1] = 13 - 1 = 12`

Implementation:

```cpp
int main() {
	// Input data
	int n, q;
	cin >> n >> q;
	vector<int> a(n);
	for(int i = 0; i < n; i++) {
		cin >> a[i];
	}

	// Generate prefix sum array
	vector<int> p(n);
	p[0] = a[0];
	for(int i = 1; i < n; i++) {
		p[i] = p[i - 1] + a[i];
	}

	// Handle queries
	for(int i = 0; i < q; i++) {
		int l, r;
		cin >> l >> r;
		l--;
		int ans = p[r] - (l == 0 ? 0 : p[l - 1]);
		cout << ans << "\n";
	}
}
```

```python
# Input data
n, q = map(int, input().split())
a = list(map(int, input().split()))

# Generate prefix sum array
p = [0] * n
p[0] = a[0]
for i in range(1, n):
	p[i] = p[i - 1] + a[i]

# Handle queries
for _ in range(q):
	l, r = map(int, input().split())
	l -= 1
	ans = p[r] - (0 if l == 0 else p[l - 1])
	print(ans)
```
