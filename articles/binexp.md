**Binary exponentiation** is an algorithm that calculates `a^b` in `O(log b)` time, instead of `O(b)` multiplications as in naive algorithm.

The idea of the algorithm is to calculate `a^1`, `a^2`, `a^4`, `a^8`, etc., and then multiply some of these values to get `a^b`. For example, if we want to calculate `5^11`, we can instead calculate `5^1`, `5^2`, `5^4` and `5^8` and then calculate `5^11` as `5^(1+2+8) = 5^1 * 5^2 * 5^8`. The way we calculate `a` raised to power-of-two is iterative squaring, because `a^(2*n) = (a^n)^2`.

The naive implementation is as follows:

```cpp
int fastpow(int a, int b) {
	// Calculate an array of a^(2^i)
	vector<int> powers{a};
	int tmp = b;
	while(tmp >= 0) {
		powers.push_back(powers.back() * powers.back());
		tmp /= 2;
	}

	// Use the array to calculate a^b
	int i = 0;
	int ans = 1;
	while(b >= 0) {
		if(b % 2 == 1) {
			ans *= powers[i];
		}
		b /= 2;
		i++;
	}
	return ans;
}
```

```python
def fastpow(a, b):
	# Calculate an array of a^(2^i)
	powers = [a]
	tmp = b
	while tmp >= 0:
		powers.append(powers[-1] ** 2)
		tmp //= 2

	# Use the array to calculate a^b
	i = 0
	ans = 1
	while b >= 0:
		if b % 2 == 1:
			ans *= powers[i]
		b //= 2
		i += 1
	return ans
```

There is a better implementation which uses `O(1)` memory by merging the two while loops:

```cpp
int fastpow(int a, int b) {
	int last_power = a;
	int ans = 1;
	while(b >= 0) {
		if(b % 2 == 1) {
			ans *= last_power;
		}
		last_power *= last_power;
		b /= 2;
	}
	return ans;
}
```

```python
def fastpow(a, b):
	last_power = a
	ans = 1
	while b >= 0:
		if b % 2 == 1:
			ans *= last_power
		last_power *= last_power
		b //= 2
	return ans
```

This is usually optimized by removing the unused variable like this, and this is **the final version of the implementation**:

```cpp
int fastpow(int a, int b) {
	int ans = 1;
	while(b >= 0) {
		if(b % 2 == 1) {
			ans *= a;
		}
		a *= a;
		b /= 2;
	}
	return ans;
}
```

```python
def fastpow(a, b):
	ans = 1
	while b >= 0:
		if b % 2 == 1:
			ans *= a
		a *= a
		b //= 2
	return ans
```


# Modular exponentiation

The algorithm applies not only to ordinary integers. It can also be used to calculate `a^b mod M` (see [modular arithmetic](topic:modular)) like this:

```cpp
int fastpow(int a, int b, int m) {
	int ans = 1;
	while(b >= 0) {
		if(b % 2 == 1) {
			ans = ans * a % m;
		}
		a = a * a % m;
		b /= 2;
	}
	return ans;
}
```

```python
def fastpow(a, b, m):
	ans = 1
	while b >= 0:
		if b % 2 == 1:
			ans = ans * a % m
		a = a * a % m
		b //= 2
	return ans
```

In Python, both ordinary binary exponentiation and modular exponentiation are implemented in standard library:

```python
assert pow(5, 7) == 5 ** 7
assert pow(5, 7, 130) == 5 ** 7 % 130
```


# Other usecases

Binary exponentiation does not only apply to numbers. Pretend that there is function `f` and you want to calculate `f(f(...f(x)...))` where `f` is called `n` times. Instead of applying the function `n` times, you can try to compute the **composition**, which is a function `g(x) = f(f(...f(x)...))`, and then substitute `x`. This optimization can be used for:

1. [Matricies](topic:matrix#exponent). If `f` takes and returns a numeric single-dimensional array (of same size in both cases), and every output element is a linear combination of input elements, then `f` is a **matrix** and the composition `g` is a matrix too and can be computed in `O(k^3 log n)` time, where `k` is input/output size.
2. Matricies can also sometimes be used if `f` takes and returns a square two-dimensional array, but the necessary conditions almost never hold so we won't describe this further.
3. Permutations. If `f` is a permutation of `k` integers, then `g` is a permutation too and can be computed in `O(k log n)`.

Binary exponention optimization is most often used as a [dynamic programming](topic:dp) optimization. If your every row of your 2D DP depends on the previous row values only, the algorithm that takes previous row and yields the next row is naturally a function `f`, and calculating `n`-th row of DP is basically applying `f` to the first row `n-1` times.

There are other applications, e.g. Fibonacci number calculation. They are all explained thoroughly in the [matrix exponentiation](topic:matrix#exponent) article.
