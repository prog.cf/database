**Bitset** is an optimized array of booleans. Compared to array of bools (`|bool[]|`), bitset requires 8 times less memory, because each element takes one bit, not one byte.

In C++, bitset is implemented in standard library in `|<bitset>|` header and can be used as follows:

```cpp
#include <bitset>

using namespace std;

bitset<1000000> arr; // Bitset size, must be constant

int main() {
	// You can now use the bitset as a simple array
	arr[5] = 1; // Set 5th element to true
	arr[5] = 0; // Set 5th element to false
	if(arr[5]) { // Read 5th element
		cout << "Yes" << endl;
	}
}
```

In Python, there is no built-in bitset, but it can be simulated (albeit slowly) using byte arrays:

```python
arr = bytearray(1000000 // 8)

def set(i, x):
	arr[i // 8] |= 1 << (i & 7)

def get(i):
	return (arr[i // 8] >> (i & 7)) & 1

set(5, 1) # Set 5th element to True
set(5, 0) # Set 5th element to False
if get(5): # Read 5th element
	print("Yes")
```
