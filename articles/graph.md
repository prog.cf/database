**Graphs** are a mathematical abstraction of a map, where graph **vertexes** correspond to geographical places and **edges** determine if you can move from one place to another.

```graph
1 2 3 4 5
1--2
2--3
3--4
1--3
4--5
2--5
```

Graphs are usually visualized as shown on this picture. Here the vertexes are labeled 1, 2, 3, 4, and 5, and there are six edges between six pairs of vertexes. You are only allowed to move from a vertex to another vertex via an edge. For instance, you can move 3 to 4, but not from 3 to 5.

A **walk** in a graph is a sequence of vertexes, such that every vertex in the sequence is connected to the next one with an edge. For instance, 1 3 4 5, 2 1 3, and 1 2 3 1 3 are walks, and 1 2 4 is not, because edge `2--4` does not exist.

A **path** is a walk without repeating vertexes. From the previous examples, 1 3 4 5 and 2 1 3 are paths, but 1 2 3 1 3 is not a path. A **cycle** is a walk where the first vertex is the same as the last one, and there are no other repetions. For instance, 1 2 3 1 is a cycle (sometimes represented without duplication: 1 2 3), while 1 3 4 5 and 1 2 3 1 3 are not cycles.

Graphs may be directed and undirected. The image above shows an undirected graph. The difference is that in an directed graph, edges have orientation, so you can only move from vertex `u` to vertex `v` if edge from u to v exists. Consider:

```graph
1 2 3 4 5
1->2
2->3
3->4
1->3
4->5
2->5
```

In this graph, 1 3 4 5 is a walk, but 2 1 3 is not, because there is no edge `2->1`.

An **acyclic** graph is a graph without cycles. An undirected acyclic graph is called a [**tree**](topic:tree), and a directed acyclic graph is called a [**DAG**](topic:dag).

---

There are weighted graphs (which may be both directed and undirected), where edges or vertexes have some cost assigned to them. That is, you pay a specific amount of money to traverse an edge or to visit a vertex.

```graph
1 2 3 4 5
1--2(1)
2--3(2)
3--4(3)
1--3(1)
4--5(5)
2--5(7)
```

In this example, edges are weighted. You can get from vertex 2 to 5 by paying 7, and from 3 to 5 by paying 8 (`3--4` costs 3 and then `4--5` costs 5).

```graph
1(3) 2(1) 3(5) 4(2) 5(4)
1--2
2--3
3--4
1--3
4--5
2--5
```

In this example, vertexes are weighted. The single-edge walk from vertex 2 to 5 costs 5 (vertex 2 costs 1 and 5 costs 4), and the two-edge walk from 3 to 5 costs 11 (3 costs 5, 4 costs 2, 5 costs 4).

A common problem on graphs is going from one vertex to another via the shortest path: [see more](problem:singlesourcedist). In weighted graphs, the path length is usually the sum of weights of used edges for edge-weighted graphs, and the sum of weights of vertexes for vertex-weighted graphs. In unweighted graphs, the path length is the count of edges in the path.

Other graph concepts:

- [topic:tree]
- [problem:addconnectivity]
- [problem:euleriancycle]
- [problem:singlesourcedist]
- [problem:allpairsdist]
- [problem:dcp]
- [problem:bridges]
- [problem:cutpoints]
- [problem:mst]
- [problem:scc]
- [problem:condensation]
