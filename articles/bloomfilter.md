**Bloom filter** is a data structure that solves the following problem:

- [problem:probset]

This problem can be solved deterministically by storing each element in a hashtable, but its probabilistic nature allows reducing memory usage.

The basic idea is to store several hashtables without collision checks, i.e. not closed/open addressing, but simply use `H(x)` as hashtable index. This may result in collisions, but if several hashtables are used, the collision probability falls significantly.

Bloom filter stores `k` arrays of `m` bits each `A_1, A_2, ..., A_k` and uses `k` different [hash functions](topic:hash) `H_1` to `H_k`. The bit arrays are initially empty. When element `x` is added, bloom filter sets `A_i[H_i(x)] = 1` for all `i` from `1` to `k`. Then, when queried for `x`, bloom filter responds 'most likely present' if `A_i[H_i(x)]` is set for all `i`, and 'surely not present' if any of those `k` bits is zero.

Here is an implementation of Bloom filter for strings of lowercase English letters, giving 1% false positive error rate.

```cpp
const int M = 9599977; // this is a prime
const int K = 7;

class BloomFilter {
	array<bitset<M>, K> _data;

	int calc_hash(const string& s, int idx) const {
		int base = 26 + idx;
		int hash = 0;
		for(char c: s) {
			hash = ((long long)hash * base + (c - 'a')) % M;
		}
		return hash;
	}

public:
	void insert(const string& x) {
		for(int i = 0; i < K; i++) {
			data[i][calc_hash(x, i)] = true;
		}
	}

	bool count(const string& x) const {
		for(int i = 0; i < K; i++) {
			if(!data[i][calc_hash(x, i)]) {
				return false;
			}
		}
		return true;
	}
};
```

```python
M = 9599977 # this is a prime
K = 7

class BloomFilter:
	def __init__(self):
		self._data = [bytearray((M + 1) // 8) for _ in range(K)]

	def _calc_hash(self, s, idx):
		base = 26 + idx
		hash = 0
		for c in s:
			hash = (hash * base + (ord(c) - ord("a"))) % M
		return hash

	def add(self, x):
		for i in range(K):
			hash = self._calc_hash(x, i)
			data[i][hash // 8] |= 1 << (hash % 8)

	def __contains__(self, x):
		for i in range(K):
			hash = self._calc_hash(x, i)
			if ((data[i][hash // 8] >> (hash % 8)) & 1) == 0:
				return False
		return True
```

The collision analysis is like this. The probabilty of false positive response is `p = (1 - (1 - 1/m)^k)^k`, where `n` is the count of elements in the set. For large `m`, this is equal to `p = (1 - e^(-kn/m))^k`. If we choose `k` to minimize `p` for fixed `n` and `m`, we get `k = m/n * ln(2)`. It can be shown that for optimal `k` and fixed `n` and `p`, `m = -1.44 n * log_2(p)`. Then, for fixed `p`, it's obvious that `k = -log_2(p)`, note that this formula doesn't depend on `n` and `m`.

As a rule of thumb, if you want 1% error rate, `k` should be about 7 and `m` should be about `9.6 * n`. If you're using polynomial hashing, it is better to make `m` the nearest prime to `9.6 * n`.
