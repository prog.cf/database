The [knapsack problem](problem:knapsack), as every NP-complete problem, can be solved in exponential time by bruteforcing the things you add to the knapsack.

The simplest implementation is as follows:

```cpp
// The first item of the pair is weight, the second is cost
vector<bool> knapsack(vector< pair<int, int> > things, int weight_limit) {
	int ans_cost = 0;
	int ans_mask = 0;
	// Bruteforce all masks. If i-th bit is set, we take the item.
	for(int mask = 0; mask < (1 << things.size()); mask++) {
		// Calculate total weight and cost of the taken items
		int taken_weight = 0;
		int taken_cost = 0;
		for(int i = 0; i < things.size(); i++) {
			if((mask >> i) & 1) {
				taken_weight += things[i].first;
				taken_cost += things[i].second;
			}
		}
		// Update the items fit in the knapsack and the answer if better
		if(taken_weight <= weight_limit && ans_cost >= taken_cost) {
			ans_cost = taken_cost;
			ans_mask = mask;
		}
	}
	// Convert bitmask to a boolean array
	vector<bool> answer(things.size());
	for(int i = 0; i < things.size(); i++) {
		answer[i] = (ans_mask >> i) & 1;
	}
	return answer;
}
```

```python
# The first item of the pair is weight, the second is cost
def knapsack(things, weight_limit):
	ans_cost = 0
	ans_mask = 0
	# Bruteforce all masks. If i-th bit is set, we take the item.
	for mask in range(1 << len(things)):
		# Calculate total weight and cost of the taken items
		taken_weight = sum(thing[0] for i, thing in enumerate(things) if (mask >> i) & 1)
		taken_cost = sum(thing[1] for i, thing in enumerate(things) if (mask >> i) & 1)
		# Update the items fit in the knapsack and the answer if better
		if taken_weight <= weight_limit && ans_cost >= taken_cost:
			ans_cost = taken_cost
			ans_mask = mask
	# Convert bitmask to a boolean array
	return [bool((ans_mask >> i) & 1) for i in range(len(things))]
```

The time complexity is `O(2^n*n)`: `2^n` masks are iterated, and each is checked in `O(n)`. The space complexity is obviously `O(n)`.

This can further be optimized to `O(2^n)` time complexity by using recursion or [Grey code approach](topic:greycode).
