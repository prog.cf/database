**MEX (Minimum Excluded)** of a set is the minimum positive integer that is not present in the set. For example:

- `MEX({1, 2, 3, 5, 7}) = 4`
- `MEX({2, 4}) = 1`
- `MEX({}) = 1`

MEX of an sorted set can be calculated in `O(ans)` as follows:

```cpp
int mex(const vector<int>& arr) {
	int ans = 1;
	while(ans <= arr.size() && arr[ans - 1] == ans) {
		ans++;
	}
	return ans;
}
```

```python
def mex(arr):
	ans = 1
	while ans <= len(arr) and arr[ans - 1] == ans:
		ans += 1
	return ans
```

MEX of an unordered array can only be computed in `O(n log n)` by sorting it first and then applying the algorithm above.

Range MEX queries can be efficiently answered using Mo's algorithm: [algo:mo#rangemex].
