**Hashing** is a common method which is often used for time and memory optimization.

The core idea is to define a **hash function** `H`, which maps some objects to integers in a fixed range. The hash function satisfies the following conditions:

1. `X = Y => H(X) = H(Y)`, that is, equal objects have equal hashes,
2. `H(X) = H(Y) => \(likely) X = Y`, that is, if two hashes are equal, their objects are most likely equal too.

It is obvious that an infinite count of objects cannot be uniquely hashed into a fixed integer, so collisions may happen. A **collision** is a situation when two different objects have the same hashes.

There are many ways to calculate hashes, but let's take a look at polynomial hashing first.


# polynomial:Polynomial hashing

The **polynomial hash** of string `s = a_1 a_2 a_3 ... a_n` is defined as `H(s) = (b^(n-1)a_1 + b^(n-2)a_2 + b^(n-3)a_3 + ... + b^(0)a_n) mod p`, where `b` is called **base** and is at least the size of the alphabet and `p` is some big prime called **modulus**.

Here is an example:

- `s = "caba"`
- There are only three different characters, so let's choose base `b = 3`.
- Let's also omit the modulo operation for the sake of example.
- `a = [2, 0, 1, 0]`
- `H(s) = 3^3*2 + 3^2*0 + 3^1*1 + 3^0*0 = 27*2 + 9*0 + 3*1 + 1*0 = 57`

Here is intuition behind using polynomial hashing. If you view `a_1 a_2 a_3 ... a_n` as an integer in base `b`, then `b^(n-1) a_1 + b^(n-2) a_2 + b^(n-3) a_3 + ... + b^0 a_n` is in fact the corresponding number. Compare: if `b = 10`, i.e. base 10, and `a = [1, 2, 3]`, then `H(s) = 123`. Then, we take this number modulo `p`, just to fit it into a fixed count of bits.

Here is the code to calculate a hash of a given string with 26-letter alphabet, from `a` to `z`. We use base 26 and modulus `10^9+7`, which is the minimum prime above one billion. Other common moduli are `10^9+9` and `998244353`.

```cpp
const int BASE = 26;
const int MOD = 1000000007;

int hash_string(const string& s) {
	int hash = 0;
	for(char c: s) {
		hash = ((long long)hash * BASE + (c - 'a')) % MOD;
	}
	return hash;
}
```

```python
BASE = 26
MOD = 1000000007

def hash_string(s):
	hash = 0
	for c in s:
		hash = (hash * BASE + (ord(c) - ord("a"))) % MOD
	return hash
```

Let's analyse the probabilty of collision in polynomial hashing. A random string gives a random integer, so the probability that any two random strings have the same hash is obviously `1/p`.

This does not explain why prime moduli are used. Here is a mathematical proof why `p` should be prime; you might not understand it, if you don't, just skip this paragraph. Pretend that you have a string `s = a_1 a_2 a_3 ... a_n` and you want to find another string `t` which has the same hash but little difference from `s`. This is quite a common problem, because data is usually structured, so there may be many repetions between strings. Regardless, let's take `s` and replace i-th character with x, so we get `t = a_1 a_2 ... a_(i-1) x a_(i+1) ... a_n`. It is obvious that `H(t) - H(s) = (x - a_i) * b^(n-i) (mod p)`. If we require `H(s) = H(t)`, solving the equation, we get `x = a_i (mod p/gcd(p, b^(n-i)))`. This implies that if `p` and `b` have common factors, it is *more likely* that choosing `x` randomly will result in collisions, by a factor of `gcd(p, b^(n-i))`. That is why `p` and `b` should be coprime. In reality, you don't control `b` much, because the data may be generated in a special way to produce more collisions. That is why `p` is usually chosen to be prime, such that `gcd(p, b) = 1` for any `b`, to minimize the probability of collision.

## Substring hash

The next common problem is to calculate the polynomial hash of a substring of a given string. More formally:

You are given a string `s` of length `N`, `N <= 2*10^5`. You have to answer `Q` queries. In each query, you are given two numbers `l` and `r` and you must print the hash of substring of `s` starting on `l`-th character and ending on `r`-th character.

The `O(n^2)` solution is obvious, you just slice every substring and calculate its hash as above. What about `O(n)`?

The idea is similar to [prefix sums](algo:prefixsum). Let's calculate the polynomial hash of each prefix:

- `h_1 = a_1 mod p`
- `h_2 = (a_1*b + a_2) mod p`
- `h_3 = (a_1*b^2 + a_2*b + a_3) mod p`
- `h_4 = (a_1*b^3 + a_2*b^2 + a_3*b + a_4) mod p`
- ...
- `h_n = (a_1*b^(n-1) + a_2*b^(n-2) + ... + a_n*b^0) mod p`

Then, if we want to calculate hash of string `s[l:r]`, let's take a look at `h[l-1]` and `h[r]`:

- `h_l = (a_1*b^(l-2) + a_2*b^(l-3) + ... + a_(l-1)*b^0) mod p`
- `h_r = (a_1*b^(r-2) + a_2*b^(r-3) + ... + a_(r-1)*b^0) mod p`

Let's multiply `h_l` by `b^(r-l)`:

- `h_l*b^(r-l) mod p = (a_1*b^(r-2) + a_2*b^(r-3) + ... + a_(l-1)*b^(r-l)) mod p`
- `h_r = (a_1*b^(r-2) + a_2*b^(r-3) + ... + a_(r-1)*b^0) mod p`

Notice that now the are many common terms at the right side of the two equations. If we subtract the first equation from the second one, we get:

`(h_r - h_l*b^(r-l)) mod p = (a_l*b^(r-l-1) + a_2*b^(r-3) + ... + a_(r-1)*b^0) mod p`

Which is exactly the hash of `s[l:r]`! In other words, `H(s[l:r]) = (h_r - h_l*b^(r-l)) mod p`.

Problem solution, `O(n)`:

```cpp
const int BASE = 26;
const int MOD = 1000000007;

int main() {
	int n, q;
	cin >> n >> q;

	string s;
	cin >> s;

	// Calculate prefix hashes
	vector<int> h(n);
	for(int i = 0; i < n; i++) {
		h[i] = ((i == 0 ? 0 : h[i - 1]) * BASE + (s[i] - 'a')) % MOD;
	}

	// Calculate powers of base
	vector<int> pow(n + 1);
	pow[0] = 1;
	for(int i = 1; i <= n; i++) {
		pow[i] = pow[i - 1] * BASE % MOD;
	}

	// Answer queries
	for(int i = 0; i < q; i++) {
		int l, r;
		cin >> l >> r;
		l--; r--;
		// Adding P and then taking modulo P is to account for negative values
		int hash = (h[r] - (long long)(l == 0 ? 0 : h[l - 1]) * pow[r - l] % MOD + MOD) % MOD;
		cout << hash << "\n";
	}

	return 0;
}
```

```python
BASE = 26
MOD = 1000000007

n, q = map(int, input().split())

s = input()

# Calculate prefix hashes
h = [0] * n
for i in range(n):
	h[i] = ((0 if i == 0 else h[i - 1]) * BASE + (ord(s[i]) - ord("a"))) % MOD

# Calculate powers of base
pow = [0] * (n + 1)
pow[0] = 1
for i in range(1, n + 1):
	pow[i] = pow[i - 1] * BASE % MOD

# Answer queries
for _ in range(q):
	l, r = map(int, input().split())
	l -= 1
	r -= 1
	hash = (h[r] - (l == 0 ? 0 : h[l - 1]) * pow[r - l]) % MOD;
	print(hash)
```
