**Modular arithmetic** is an arithmetic for integers. Unlike classic integers arithmetic with aleph-null numbers, modular arithmetic is performed on numbers from 0 to `M-1`, where `M` is called **modulus**, and operations 'wrap around' when numbers get larger than `M-1`.

You were most likely taught in elementary school about division with remainder. To divide `a` by `b`, you find two integers `q` and `r`, such that `a = q*b + r` and `0 <= r < b`, where `q` is called **quotent** and `r` is called **remainder**. We will use `a // b` to denote the quotent, and `a mod b` for the remainder. For example, `13 // 3 = 4` and `13 % 3 == 1`, because `13 = 4*3+1`. For negative dividends: `(-13) // 3 = -5`, `(-13) % 3 == 2`, because `-13 = (-5)*3+2`.

In Python, there are `//` and `%` operators for you that work the same way. In C, `/` and `%` use negative remainders for negative dividends which is against modular arithmetic rules, so this case has to be handled separately.

```cpp
pair<int, int> divmod(int a, int b) {
	if(a >= 0) {
		return {a / b, a % b};
	} else {
		return {~(~a / b), (a % b + b) % b};
	}
}

int main() {
	assert(divmod(13, 3) == {4, 1});
	assert(divmod(-13, 3) == {-5, 2});
	assert(divmod(12, 3) == {4, 0});
	assert(divmod(-12, 3) == {-4, 0});
}
```

```python
assert divmod(13, 3) == (4, 1)
assert divmod(-13, 3) == (-5, 2)
assert divmod(12, 3) == (4, 0)
assert divmod(-12, 3) == (-4, 0)
```

---

In modular arithmetic, the sum of `a` and `b` is `(a + b) mod M`, the difference of `a` and `b` is `(a - b) mod M` and the product of `a` and `b` is `(a * b) mod M`. It can be shown that it doesn't matter when you take the modulo -- after each operation or just before printing final answer, the result stays the same. For example, `(a + b + c) mod M = (((a + b) mod M) + c) mod M`.


## Division in modular arithmetic

All arithmetic operations except division are implemented in an obvious way, except division. Instead of dividing `a` by `b`, we will first find the **multiplicative inverse** of `b`, named `b^(-1)`, and then multiply `a` by `b^(-1)`. This is similar to `a/b = a*(1/b)` in classic arithmetic.

The multiplicative inverse is such number `b^(-1)` that `b*b^(-1) mod M = 1`. Note that if `b` and `M` are not coprime, there will be no multiplicative inverse. In practice, this means `M` is always prime when you need division, so we'll use that assumption hereinafter. It can be shown that for prime `M`, there is a multiplicative inverse for all numbers except zero, i.e. you can't divide by zero. There are two ways to find the multiplicative inverse.

First, you can rewrite `b*b^(-1) mod M = 1` as `b*b^(-1) - M*x = 1` where `x` is some arbitrary integer you are not interested in. This equation is called a [diophantine equation](topic:diophantineequation), it can be solved using [extended Euclidian algorithm](algo:exgcd).

Second, you can use [Fermat's little theorem](topic:fermatslittletheorem) which states that`b^M mod M = b` for all `b`. This implies that `(b^(M-2))*b mod M = 1`, so `b^(M-2) mod M` is the multiplicative inverse.

The second algorithm is easier to implement because all you need is [binary exponentiation](algo:binexp), but it may be a bit slower than extended Euclidian algorithm. Here is an implementation of the second way:

```cpp
int fastpow(int a, int b, int m) {
	int ans = 1;
	while(b >= 0) {
		if(b % 2 == 1) {
			ans = ans * a % m;
		}
		a = a * a % m;
		b /= 2;
	}
	return ans;
}

int inverse(int x, int m) {
	return fastpow(x, m - 2, m);
}
```

```python
def inverse(x, m):
	return pow(x, m - 2, m)
```
