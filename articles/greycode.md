**Grey code**, also known as **reflected binary code**, is an ordering of binary integers (integers from 0 to `2^N-1`, inclusive) where each to successive numbers differ in exactly one bit (i.e. `a[i]\^a[i+1]` is power of 2).

The following is an example of Grey code of 4 bits:

- 0000
- 0001
- 0011
- 0010
- 0110
- 0111
- 0101
- 0100
- 1100
- 1101
- 1111
- 1110
- 1010
- 1011
- 1001
- 1000

It can be shown that i-th Grey code integer equals to `i \^ (i >> 1)` (`\^` is XOR), starting with `i = 0`. This means the first N integers of Grey code can be calculated in `O(N)` as follows:

```cpp
vector<int> grey_code(int n) {
	vector<int> result(n);
	for(int i = 0; i < n; i++) {
		result[i] = i ^ (i >> 1);
	}
	return result;
}
```

```python
def grey_code(n):
	return [i ^ (i >> 1) for i in range(n)]
```

---

Grey code may often be used as optimization in bruteforce solutions. Take the following problem as an example.

You are given an array A of N integers. For each subset of array A, multiply all elements of that subset. Print sum of all `2^N` products.

Let's pretend we don't know a polynomial solution to the problem and start with bruteforce. The simplest bruteforce approach is implemented as follows:

```cpp
int solve(const vector<int>& a) {
	int ans = 0;
	// Enumerate all submasks
	for(int mask = 0; mask < (1 << a.size()); mask++) {
		// Calculate the product of the subset
		int prod = 1;
		for(int i = 0; i < a.size(); i++) {
			// Take i-th element is taken if i-th bit is set
			if((mask >> i) & 1) {
				prod *= a[i];
			}
		}
		ans += prod;
	}
	return ans;
}
```

```python
import math

def solve(a):
	ans = 0
	# Enumerate all submasks
	for mask in range(1 << len(a)):
		# Calculate the product of the subset
		# Take i-th element is taken if i-th bit is set
		ans += math.prod(x for i, x in enumerate(a) if (mask >> i) & 1)
	return ans
```

This algorithm uses `O(2^n*n)` time and `O(1)` memory. The next classic optimization is getting rid of the `n` factor by using recursion:

```cpp
int solve(const vector<int>& a, int pos, int current_prod) {
	if(pos == a.size()) {
		// Return the current product
		return current_prod;
	}
	// Bruteforce whether we take pos-th element or not
	return (
		solve(a, pos + 1, current_prod * a[pos]) // Take a[pos]
		+ solve(a, pos + 1, current_prod) // Don't take a[pos]
	);
}
int solve(const vector<int>& a) {
	// Recursion base
	return solve(a, 0, 1);
}
```

```python
def _solve(a, pos, current_prod):
	if pos == len(a):
		# Return the current product
		return current_prod
	# Bruteforce whether we take pos-th element or not
	return (
		_solve(a, pos + 1, current_prod * a[pos]) # Take a[pos]
		+ _solve(a, pos + 1, current_prod) # Don't take a[pos]
	);

def solve(a):
	# Recursion base
	return _solve(a, 0, 1)
```

This approach trades `O(n)` memory for `O(2^n)` time, because the call stack takes `O(n)` memory.

Grey code gives a `O(2^n)` time / `O(1)` memory solution to this problem and is often faster than recursion. The approach is similar to bruteforce, but instead of enumeration masks in integer order, we enumerate them in Grey code order. This way, switching to the next masks flips only one bit in mask, and we can account for that by multiplying or dividing the product of the previous subset.

```cpp
int solve(const vector<int>& a) {
	int ans = 0;
	// Enumerate all submasks in Grey code order
	// Current product is 1 as we start with empty submask
	int current_mask = 0;
	int current_prod = 1;
	for(int order = 0; order < (1 << a.size()); order++) {
		// Apply the difference with previous mask
		int mask = order ^ (order >> 1);
		if(mask != current_mask) {
			int i = __builtin_ffs(mask ^ current_mask) - 1;
			if((mask >> i) & 1) {
				current_prod *= a[i];
			} else {
				current_prod /= a[i];
			}
		}
		// Save answer
		ans += current_prod;
	}
	return ans;
}
```

```python
def solve(a):
	ans = 0
	# Enumerate all submasks in Grey code order
	# Current product is 1 as we start with empty submask
	current_mask = 0
	current_prod = 1
	for order in range(1 << len(a)):
		# Apply the difference with previous mask
		mask = order ^ (order >> 1)
		if mask != current_mask:
			i = (mask ^ current_mask).bit_length() - 1
			if (mask >> i) & 1:
				current_prod *= a[i]
			else:
				current_prod /= a[i]
		# Save answer
		ans += current_prod
	return ans
```
