A **tree** is an unoriented [graph](topic:graph) without cycles.

```graph
1--2
2--3
3--4
2--5
2--6
3--7
```

The following are all equivalent definitions of a tree:

- An unoriented graph without cycles.
- An unoriented graph with `N` vertexes and `N-1` edges.
- An unoriented graph where any two vertexes are connected by exactly one path.

Vertexes of a tree are usually called **nodes**.

A **rooted tree** is a tree where there is a special node called **root**. Rooted trees are usually drawn starting from the root from top to bottom:

```tree
1--2
2--3
3--4
2--5
2--6
3--7
```

A **path** in a tree is simply a sequence of vertexes where each two neighbour vertexes are connected in the tree (without repetions). For instance, 4 3 2 5 is a path in the tree above.

A **vertical path** in a rooted tree is a path where you always go 'down' the tree. For example, 2 3 4 is a vertical path and 5 2 3 is not.

Common optimization problems on trees are [path minimum query](problem:pathmin) and [path sum query](problem:pathsum), usually solved with [heavy-light decomposition](algo:hld).

Other common concepts include:

- [problem:lca]
- Distance between two vertexes can be computed in `O(log n)` time using LCA: [see more](problem:lca#distance).
- [problem:mst]
- [topic:dpontree]
